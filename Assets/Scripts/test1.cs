using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class test1 : MonoBehaviour
{
   
   
   [SerializeField] Button m_btnInsert;
   [SerializeField] Button m_btnRemovePrime;
   [SerializeField] Button m_btnRemovePerfectSquare;
   [SerializeField] Button m_btnSwitchScene;

   [SerializeField] private InputField m_inputListNumber;
   [SerializeField] private InputField m_inputIndex;
   [SerializeField] private InputField m_inputValueInsert;

   [SerializeField] TMP_Text m_txtResult;

    
   private void Start()
   {
       m_btnInsert.onClick.AddListener(ButtonInsertOnClick);
       m_btnRemovePrime.onClick.AddListener(ButtonRemovePrimeOnClick);
       m_btnRemovePerfectSquare.onClick.AddListener(ButtonRemovePerfetSquareOnClick);
       m_btnSwitchScene.onClick.AddListener(SwitchToScene2);
       // Test
   }

   private void OnDestroy()
   {
       m_btnInsert.onClick.RemoveListener(ButtonInsertOnClick);
       m_btnRemovePrime.onClick.RemoveListener(ButtonRemovePrimeOnClick);
       m_btnRemovePerfectSquare.onClick.RemoveListener(ButtonRemovePerfetSquareOnClick);
       m_btnSwitchScene.onClick.RemoveListener(SwitchToScene2);
   }

   void ButtonInsertOnClick()
   {
       string[] listNumber = m_inputListNumber.text.Split(",");
       List<int> myList = new List<int>();
       for (int i = 0; i < listNumber.Length; i++)
       {
           try
           {
                myList.Add(Int32.Parse(listNumber[i]));
           }
           catch (Exception )
           {
               Debug.Log("Please enter an integer!");
               return;
           }
       }

       if (string.IsNullOrEmpty(m_inputIndex.text) || string.IsNullOrEmpty(m_inputValueInsert.text))
       {
           Debug.Log("Input data is incorrect!");
       }
       else
       {
           if (Int32.Parse(m_inputIndex.text) < 0 || Int32.Parse(m_inputIndex.text) > myList.Count)
           {
               Debug.Log("Invalid location!");
           }
           else
           {
               try
               {
                   myList.Insert(Int32.Parse(m_inputIndex.text),Int32.Parse(m_inputValueInsert.text));
                   m_txtResult.text = string.Join(IVariable.slag,myList.ToArray());
               }
               catch (Exception )
               {
                   Debug.Log("Please enter an integer!");
               }
               
           }
           
       }



       
       
      
   }

    void ButtonRemovePrimeOnClick()
   {
       string[] listNumber = m_inputListNumber.text.Split(IVariable.separator);
       List<int> myList = new List<int>();
       for (int i = 0; i < listNumber.Length; i++)
       {
           try
           {
               if (!IsPrime(Int32.Parse(listNumber[i])))
               {
                   myList.Add(Int32.Parse(listNumber[i]));
               }
           }
           catch (Exception )
           {
               Debug.Log("Please enter an integer!");
               return;
           }
       }
       m_txtResult.text = string.Join(IVariable.slag,myList.ToArray());
   }
    
    void ButtonRemovePerfetSquareOnClick()
    {
        string[] listNumber = m_inputListNumber.text.Split(",");
        List<int> myList = new List<int>();
        for (int i = 0; i < listNumber.Length; i++)
        {
            try
            {
                if (!IsPerfectSquare(Int32.Parse(listNumber[i])))
                {
                    myList.Add(Int32.Parse(listNumber[i]));
                }
            }
            catch (Exception )
            {
                Debug.Log("Please enter an integer!");
                return;
            }
        }
        m_txtResult.text = string.Join(IVariable.slag,myList.ToArray());
    }
    static bool IsPrime(int number)
   {
       if (number < 2)
       {
           return false;
       }

       for (int i = 2; i <= Math.Sqrt(number); i++)
       {
           if (number % i == 0)
           {
               return false;
           }
       }

       return true;
   }
    static bool IsPerfectSquare(int number)
    {
        double squareRoot = Math.Sqrt(number);
        return (squareRoot % 1 == 0);
    }
    
    void SwitchToScene2()
    {
        SceneManager.LoadScene("scenes2");
    }

   
}
