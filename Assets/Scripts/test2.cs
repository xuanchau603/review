using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class test2 : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Button m_btnSwitchScene;
    [SerializeField] Button m_btnResult;

    [SerializeField] InputField m_inputNumber;
    [SerializeField] TMP_Text m_txtResult;
    void Start()
    {
        m_btnResult.onClick.AddListener(BtnSubmitOnClick);
        m_btnSwitchScene.onClick.AddListener(SwitchToScene3);
    }

    void OnDestroy()
    {
        m_btnResult.onClick.RemoveListener(BtnSubmitOnClick);
        m_btnSwitchScene.onClick.RemoveListener(SwitchToScene3);
    }

    void BtnSubmitOnClick()
    {
        m_txtResult.text = null;
        try
        {
            int n = Int32.Parse(m_inputNumber.text);
            for (int i = 1; i <= n; i++)
            {
                int fibonacciNumber = Fibonacci(i);
                m_txtResult.text +=  fibonacciNumber+IVariable.slag;
            }
        }
        catch (Exception)
        {
            Debug.Log("Please enter an integer!");
        }
    }

    
    
    
    public void SwitchToScene3()
    {
        SceneManager.LoadScene("scene3");
    }
    
    static int Fibonacci(int n)
    {
        if (n <= 2)
        {
            return 1;
        }
        else
        {
            int a = 1;
            int b = 1;
            int fibonacciNumber = 0;
            
            for (int i = 3; i <= n; i++)
            {
                fibonacciNumber = a + b;
                a = b;
                b = fibonacciNumber;
            }
            
            return fibonacciNumber;
        }
    }
}
