using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class test3: MonoBehaviour, IVariable
{
    // Start is called before the first frame update
    
    [SerializeField]  Button m_btnInverted;
    [SerializeField]  Button m_btnSwitchScene;

    [SerializeField]  InputField m_inputFirstNumber;
    [SerializeField]  InputField m_inputSecondNumber;

    [SerializeField]  TMP_Text m_txtResult;
    
    void Start()
    {
        m_btnInverted.onClick.AddListener(btnSubmitOnClick);
        m_btnSwitchScene.onClick.AddListener((SwitchToScene4));
        
    }
    void OnDestroy()
    {
        m_btnInverted.onClick.RemoveListener(btnSubmitOnClick);
        m_btnSwitchScene.onClick.RemoveListener((SwitchToScene4));
        
        Debug.Log(IVariable.slag);
    }

    // Update is called once per frame
    void btnSubmitOnClick()
    {

        try
        {
            int a = Int32.Parse(m_inputFirstNumber.text);
            int b = Int32.Parse(m_inputSecondNumber.text);
            // int temp = a;
            // a = b;
            // b = temp;
        
            // (a, b) = (b, a);
        
            a = a ^ b;
            b = a ^ b;
            a = a ^ b;

        
            m_txtResult.text = "A: " + a + "-B: " + b;
        }
        catch (Exception )
        {
            Debug.Log("Plear enter an integer!");
        }
    }
     void SwitchToScene4()
    {
        SceneManager.LoadScene("scene4");
    }
}
